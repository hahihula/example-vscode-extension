import * as vscode from 'vscode';

let myStatusBarItem: vscode.StatusBarItem;

export function activate(context: vscode.ExtensionContext) {

	myStatusBarItem = vscode.window.createStatusBarItem(vscode.StatusBarAlignment.Left, 300);
	
	let created = vscode.workspace.onDidCreateFiles(() => {
		getNumberOfXMLFiles(updateStatusBarItem);
	});

	let deleted = vscode.workspace.onDidDeleteFiles(() => {
		getNumberOfXMLFiles(updateStatusBarItem);
	});
	context.subscriptions.push(myStatusBarItem , created, deleted);
	getNumberOfXMLFiles(updateStatusBarItem);
}

export function deactivate() { }


function updateStatusBarItem(n: number): void {
	myStatusBarItem.text = `XML: ${n}`;
	myStatusBarItem.show();
}

function getNumberOfXMLFiles(updateItem: Function): void {
	let files = 0;
	vscode.workspace.findFiles('**/*.xml').then((f) => {
		files = f.length;
		updateItem(files);
	});
}